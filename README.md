# ExamAutoSolution

#### 项目介绍
自动完成优课(YOOC)平台上课群的在线考试，基于SQLite数据库

#### 软件架构

**core**：核心刷题代码模块  
**client**: 基于jcef的交互模块

##### 主要依赖三方库
**retrofit2**: 负责主要的网络访问  
**jsoup**: 负责部分html解析  
**mybatis**: 负责连接与操作数据库  
**jcef**:负责可视化操作，用html渲染界面

#### 使用说明

```java
    ExamSolution examSolution = new ExamSolution("username", "password");
    examSolution.login();
    examSolution.work("gourpId"); // 设置课群ID
```

#### 程序截图

![img](/img/1.png)

![img](/img/2.jpg)

![img](/img/3.png)

![img](/img/4.png)

![img](/img/5.png)

#### 参与贡献

1. Fork 本项目
2. 新建 feature-xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)