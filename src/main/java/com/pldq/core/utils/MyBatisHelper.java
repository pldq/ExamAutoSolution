package com.pldq.core.utils;

import com.pldq.core.mapper.ApiMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.*;
import java.sql.SQLException;

/**
 * @author pldq
 * @date 2018-11-18
 */
public class MyBatisHelper implements Closeable {

    private static final String FILENAME_DATABASE = "data.db";
    private static final int SIZE_BUFFER = 256;

    private static MyBatisHelper myBatisHelper;
    private SqlSession sqlSession;

    private static void exportDatabase() {
        File database = new File(FILENAME_DATABASE);
        if (database.exists()) {
            return;
        }
        try (FileOutputStream outputStream = new FileOutputStream(database)) {
            InputStream inputStream = Resources.getResourceAsStream(FILENAME_DATABASE);
            byte[] buffer = new byte[SIZE_BUFFER];
            int length;
            while ((length = inputStream.read(buffer, 0, buffer.length)) != -1) {
                outputStream.write(buffer, 0, length);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private MyBatisHelper() {
        exportDatabase();
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream("mybatis-config.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder()
                .build(inputStream);
        sqlSessionFactory.getConfiguration().addMapper(ApiMapper.class);
        sqlSession = sqlSessionFactory.openSession(true);
    }

    public static MyBatisHelper getInstance() {
        if (myBatisHelper != null) {
            return myBatisHelper;
        }
        myBatisHelper = new MyBatisHelper();
        return myBatisHelper;
    }

    public<T> T getMapper(Class<T> mapperClass) {
        return sqlSession.getMapper(mapperClass);
    }

    @Override
    public void close() throws IOException {
        sqlSession.clearCache();
        sqlSession.close();

        File database = new File(FILENAME_DATABASE);
        if (database.exists() && database.isFile()) {
            //noinspection ResultOfMethodCallIgnored
            database.delete();
        }
        myBatisHelper = null;
    }
}
