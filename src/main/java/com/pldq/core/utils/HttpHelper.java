package com.pldq.core.utils;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.*;
import java.security.cert.CertificateFactory;
import java.util.concurrent.TimeUnit;

/**
 * @author pldq
 * @date 2018-11-18
 */
public final class HttpHelper {

    private static final boolean DEBUG = false;

    private static OkHttpClient client;

    private static CookieManager cookieManager;
    private static SSLSocketFactory sslSocketFactory;
    private static X509TrustManager trustManager;

    private static void initSSL() throws Exception {
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStore.load(null);
        String certificateAlias = Integer.toString(0);
        keyStore.setCertificateEntry(certificateAlias, certificateFactory.generateCertificate(
                HttpHelper.class.getResourceAsStream("/FiddlerRoot.cer")
        ));
        SSLContext sslContext = SSLContext.getInstance("TLS");
        final TrustManagerFactory trustManagerFactory =
                TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(keyStore);
        sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
        sslSocketFactory = sslContext.getSocketFactory();
        trustManager = (X509TrustManager) trustManagerFactory.getTrustManagers()[0];
    }

    private static OkHttpClient.Builder withFiddlerProxy(OkHttpClient.Builder builder) throws Exception {
        if (sslSocketFactory == null) {
            initSSL();
        }
        return builder.proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(8888)))
                .hostnameVerifier((s, sslSession) -> true)
                .sslSocketFactory(sslSocketFactory, trustManager);
    }

    private static OkHttpClient.Builder withTimeOut(OkHttpClient.Builder builder) {
        return builder.connectTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES);
    }

    private static OkHttpClient.Builder withCookieManager(OkHttpClient.Builder builder) {
        return builder.addInterceptor(chain -> {
            Request request = cookieManager.updateCookie(chain.request());
            Response response = chain.proceed(request);
            cookieManager.analyzeCookie(response.headers());
            return response;
        });
    }

    public static OkHttpClient getOkHttpClient() {
        if (client != null) {
            return client;
        }
        cookieManager = new CookieManager();
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder = withTimeOut(builder);
        builder = withCookieManager(builder);
        if (DEBUG) {
            try {
                builder = withFiddlerProxy(builder);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        client = builder.build();
        return client;
    }

    public static String getToken() {
        return cookieManager.getToken();
    }
}
