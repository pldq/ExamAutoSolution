package com.pldq.core.utils;

import okhttp3.Headers;
import okhttp3.Request;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author pldq
 * @date 2018-11-18
 */
final class CookieManager {

    private static final Pattern PATTERN_TOKEN = Pattern.compile("csrftoken=(.*?);");
    private static final Pattern PATTERN_SESSION = Pattern.compile("sessionid=(.*?);");

    private static final String HEADERS_TOKEN = "X-CSRFToken";

    private String token;
    private String sessionId;

    void analyzeCookie(Headers headers) {
        List<String> values = headers.values("Set-Cookie");
        for (String value : values) {
            Matcher matcher = PATTERN_TOKEN.matcher(value);
            if (matcher.find()) {
                if (token == null || !token.equals(matcher.group(1))) {
                    token = matcher.group(1);
                    System.out.println("token = " + token);
                }
                continue;
            }
            matcher = PATTERN_SESSION.matcher(value);
            if (matcher.find()) {
                if (sessionId == null || !sessionId.equals(matcher.group(1))) {
                    sessionId = matcher.group(1);
                    System.out.println("sessionid = " + sessionId);
                }
            }
        }
    }

    Request updateCookie(Request request) {
        StringBuilder cookie = new StringBuilder();
        if (token != null) {
            request = request.newBuilder()
                    .addHeader(HEADERS_TOKEN, token)
                    .build();
            cookie.append("csrftoken=")
                    .append(token)
                    .append(";");
        }

        if (sessionId != null) {
            cookie.append("sessionid=")
                    .append(sessionId)
                    .append(";");
        }
        request = request.newBuilder()
                .addHeader("Cookie", cookie.toString())
                .build();
        return request;
    }

    String getCookie() {
        StringBuilder cookie = new StringBuilder();
        if (token != null) {
            cookie.append("csrftoken=")
                    .append(token)
                    .append(";");
        }

        if (sessionId != null) {
            cookie.append("sessionid=")
                    .append(sessionId)
                    .append(";");
        }
        return cookie.toString();
    }

    String getToken() {
        return token;
    }
}
