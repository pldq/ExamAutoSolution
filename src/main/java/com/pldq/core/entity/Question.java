package com.pldq.core.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author pldq
 * @date 2018-11-18
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
public class Question {

    public Question() {
    }

    private String questionId;
    private String examId;
    private String answer;

}
