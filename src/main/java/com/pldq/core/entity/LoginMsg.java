package com.pldq.core.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author pldq
 * @date 2018-11-18
 */
@Getter
@Setter
public class LoginMsg {

    /**
     * success : true
     */

    private boolean success;

}
