package com.pldq.core.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Package: com.pldq.core.entity
 *
 * @author pldq
 * Created on 2018-11-27
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
public class Group {

    private String groupId;
    private String name;
    private String thumbnail;

}
