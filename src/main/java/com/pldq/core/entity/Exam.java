package com.pldq.core.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author pldq
 * @date 2018-11-18
 */
@Getter
@Setter
@AllArgsConstructor
public class Exam {

    private String id;
    private String repeat;

}
