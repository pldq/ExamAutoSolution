package com.pldq.core.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author pldq
 * @date 2018-11-18
 */
@Getter
@Setter
public class SubmitMsg {

    /**
     * html :
     * next :
     * score : 16
     * result : true
     * date :
     * message : 答案提交成功
     */

    private String html;
    private String next;
    private int score;
    private boolean result;
    private String date;
    private String message;

}
