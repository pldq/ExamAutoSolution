package com.pldq.core.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author pldq
 * @date 2018-11-18
 */
@Getter
@Setter
@ToString
public class GroupItem {
    /**
     * url : /group/24780
     * src : /media/group/f0a0b85ed62142d59fc569974bb19da3_225x130.png
     * icon : <span class="icon icon-private icon-administrative"></span>
     * title : 垃圾分类管理知识竞赛
     */

    private String url;
    private String src;
    private String icon;
    private String title;

}
