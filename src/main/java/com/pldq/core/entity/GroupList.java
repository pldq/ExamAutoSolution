package com.pldq.core.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @author pldq
 * @date 2018-11-18
 */
@Getter
@Setter
@ToString
public class GroupList {

    private int total;
    private boolean more;
    private List<GroupItem> items;

}
