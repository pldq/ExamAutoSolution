package com.pldq.core.mapper;

import com.pldq.core.entity.Question;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * @author pldq
 * @date 2018-11-18
 */
public interface ApiMapper {

    @Insert("insert into t_answer(questionId, examId, answer) VALUES (#{question.questionId}, #{question.examId}, #{question.answer})")
    int addQuestion(@Param("question") Question question);

    @Select("select answer from t_answer where examId = #{exam_id} and questionId = #{question_id}")
    String getAnswer(@Param("exam_id") String examId, @Param("question_id") String questionId);

    @Select("select count(*) from t_answer where examId = #{exam_id} and questionId = #{question_id}")
    int getQuestionCount(@Param("question_id") String questionId, @Param("exam_id") String examId);
}
