package com.pldq.core.server.converters.converter;

import okhttp3.ResponseBody;
import retrofit2.Converter;

import java.io.IOException;

/**
 * @author pldq
 * @date 2018-11-18
 */
public class StringConverter implements Converter<ResponseBody, String> {

    @Override
    public String convert(ResponseBody value) throws IOException {
        return value.string();
    }
}
