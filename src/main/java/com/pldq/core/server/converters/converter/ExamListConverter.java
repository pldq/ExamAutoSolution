package com.pldq.core.server.converters.converter;

import com.pldq.core.entity.Exam;
import okhttp3.ResponseBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import retrofit2.Converter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author pldq
 * @date 2018-11-21
 */
public class ExamListConverter implements Converter<ResponseBody, List<Exam>> {

    @Override
    public List<Exam> convert(ResponseBody value) throws IOException {
        Document document = Jsoup.parse(value.string());
        Elements elements = document.select(".clearfix");
        List<Exam> exams = new ArrayList<>(elements.size());
        for (Element element : elements) {
            String examId = element.attr("data-exam-id");
            Elements repeatElements = element.select(".repeat");
            String repeat = null;
            if (repeatElements.size() > 0) {
                repeat = repeatElements.first()
                        .attr("repeat-url")
                        .split("\\.me")[1];
            }
            exams.add(new Exam(
                    examId,
                    repeat
            ));
        }
        return exams;
    }

}
