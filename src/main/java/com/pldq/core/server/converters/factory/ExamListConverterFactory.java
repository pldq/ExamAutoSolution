package com.pldq.core.server.converters.factory;

import com.pldq.core.entity.Exam;
import com.pldq.core.server.converters.converter.ExamListConverter;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 * @author pldq
 * @date 2018-11-21
 */
public class ExamListConverterFactory extends Converter.Factory {

    public static ExamListConverterFactory create() {
        return new ExamListConverterFactory();
    }

    @Override
    public Converter<ResponseBody, ?> responseBodyConverter(Type type, Annotation[] annotations, Retrofit retrofit) {
        if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            if (parameterizedType.getRawType() == List.class &&
                    parameterizedType.getActualTypeArguments()[0] == Exam.class) {
                return new ExamListConverter();
            }
        }
        return super.responseBodyConverter(type, annotations, retrofit);
    }


}
