package com.pldq.core.server;

import com.pldq.core.entity.Exam;
import com.pldq.core.entity.GroupList;
import com.pldq.core.entity.LoginMsg;
import com.pldq.core.entity.SubmitMsg;
import com.pldq.core.server.converters.factory.ExamListConverterFactory;
import com.pldq.core.server.converters.factory.StringConverterFactory;
import com.pldq.core.utils.HttpHelper;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.*;

import java.util.List;

/**
 * @author pldq
 * @date 2018-11-18
 */
public final class ApiServer {

    private static final String SERVER_HOST = "https://www.yooc.me/";

    public static String getServerHost() {
        return SERVER_HOST;
    }

    private static RemoteApi remoteApi;

    public static RemoteApi getRemoteApi() {
        if (remoteApi != null) {
            return remoteApi;
        }
        OkHttpClient client = HttpHelper.getOkHttpClient();
        Retrofit retrofit = new Retrofit.Builder()
                .client(client)
                .addConverterFactory(StringConverterFactory.create())
                .addConverterFactory(ExamListConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(SERVER_HOST)
                .build();
        remoteApi = retrofit.create(RemoteApi.class);
        return remoteApi;
    }


    public interface RemoteApi {

        @GET("login")
        Call<String> getLoginPage();

        @POST("yiban_account/login_ajax")
        @FormUrlEncoded
        Call<LoginMsg> login(
                @Field("email") String username,
                @Field("password") String password
        );

        @GET("group/joined")
        Call<GroupList> getGroupList(
                @Query("page") int page
        );

        @POST("group/{groupId}/exam/{examId}/answer/submit")
        @FormUrlEncoded
        Call<SubmitMsg> submit(
                @Path("groupId") String groupId,
                @Path("examId") String examId,
                @Field("csrfmiddlewaretoken") String token,
                @Field("answers") String answers,
                @Field("type") int type,
                @Field("auto") int auto
        );

        @POST("{repeat}")
        @FormUrlEncoded
        Call<String> repeat(
                @Path(value = "repeat", encoded = true) String repeat,
                @Field("csrfmiddlewaretoken") String token
        );

        @GET("group/{groupId}/exam/{examId}/detail")
        Call<String> getExamContent(
                @Path("groupId") String groupId,
                @Path("examId") String examId
        );

        @GET("group/{groupId}/exams")
        Call<List<Exam>> getExamList(
                @Path("groupId") String groupId
        );
    }

}
