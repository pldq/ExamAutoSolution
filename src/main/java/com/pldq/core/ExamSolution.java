package com.pldq.core;

import com.google.gson.Gson;
import com.pldq.core.entity.*;
import com.pldq.core.mapper.ApiMapper;
import com.pldq.core.server.ApiServer;
import com.pldq.core.utils.HttpHelper;
import com.pldq.core.utils.MyBatisHelper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.Closeable;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author pldq
 * @date 2018-11-18
 */
public class ExamSolution implements Closeable {

    private String username;
    private String password;

    private ApiMapper apiMapper;

    private boolean login;

    public ExamSolution(String username, String password) {
        this.username = username;
        this.password = password;
        apiMapper = MyBatisHelper.getInstance().getMapper(ApiMapper.class);
    }

    public boolean login() throws IOException {
        ApiServer.getRemoteApi().getLoginPage().execute();
        LoginMsg msg = ApiServer.getRemoteApi().login(username, password).execute().body();
        if (msg != null && msg.isSuccess()) {
            login = true;
        } else {
            System.err.println("login failed!");
        }
        return login;
    }

    public List<Group> listGroup() throws IOException {
        List<Group> groups = new LinkedList<>();
        if (!login) {
            System.err.println("Please you login first!");
            return groups;
        }
        int page = 1;
        GroupList groupList;
        do {
            groupList = ApiServer.getRemoteApi().getGroupList(page++).execute().body();
            if (groupList == null) {
                break;
            }
            for (GroupItem item : groupList.getItems()) {
                groups.add(new Group(
                        item.getUrl().split("/")[2],
                        item.getTitle(),
                        ApiServer.getServerHost() + item.getSrc()
                ));
            }
        } while (groupList.isMore());
        return groups;

    }

    public void work(String groupId) throws IOException {
        if (!login) {
            System.err.println("Please you login first!");
            return;
        }
        for (Exam exam : Objects.requireNonNull(ApiServer.getRemoteApi().getExamList(groupId).execute().body())) {
            workOnExam(groupId, exam);
        }
    }

    private void workOnExam(String groupId, Exam exam) throws IOException {
        if (exam.getRepeat() != null) {
            ApiServer.getRemoteApi().repeat(exam.getRepeat(), HttpHelper.getToken()).execute();
        }
        boolean stop = false;
        while (!stop) {
            String content = ApiServer.getRemoteApi()
                    .getExamContent(groupId, exam.getId())
                    .execute()
                    .body();
            String answer = createSolution(exam.getId(), content);
            SubmitMsg msg = ApiServer.getRemoteApi()
                    .submit(groupId, exam.getId(), HttpHelper.getToken(), answer, 0, 0)
                    .execute()
                    .body();
            System.out.println("current score = " + Objects.requireNonNull(msg).getScore());
            if (msg.getScore() >= 100) {
                stop = true;
            } else {
                analyzeAnswer(
                        exam.getId(),
                        ApiServer.getRemoteApi().getExamContent(groupId, exam.getId()).execute().body()
                );
                for (Exam examIterator : Objects.requireNonNull(
                        ApiServer.getRemoteApi().getExamList(groupId).execute().body())) {
                    if (examIterator.getId().equals(exam.getId())) {
                        ApiServer.getRemoteApi().repeat(examIterator.getRepeat(), HttpHelper.getToken()).execute();
                    }
                }
            }
        }
    }

    private static final String QUESTION_SEPARATOR = "_";

    private String createSolution(String examId, String content) {
        List<Map> result = new LinkedList<>();

        Elements elements = Jsoup.parse(content).select(".question-board");
        for (Element element : elements) {
            Elements answerElements = element.select("input");
            String questionId = answerElements.get(0).attr("name");
            String answer = apiMapper.getAnswer(examId, questionId);

            String[] key = questionId.split(QUESTION_SEPARATOR);
            Map<String, List> answerMap = new HashMap<>(1);
            Map<String, Map> questionMap = new HashMap<>(1);

            if (answer != null) {
                answerMap.put(key[1], Arrays.asList(answer.split("\\|")));
            } else {
                answerMap.put(key[1], Collections.singletonList("1"));
            }
            questionMap.put(key[0], answerMap);
            result.add(questionMap);
        }
        return new Gson().toJson(result);
    }

    private static final Pattern PATTERN_ANSWER = Pattern.compile("正确答案：(.*?)<");
    private static final String OPTION_SEPARATOR = "、";

    private void analyzeAnswer(String examId, String content) {
        Elements elements = Jsoup.parse(content).select(".question-board");
        for (Element element : elements) {
            Elements answerElements = element.select("li");
            Matcher matcher = PATTERN_ANSWER.matcher(element.html());
            if (!matcher.find()) {
                System.err.println("can't find right answer!");
            }
            String[] results = matcher.group(1).split(OPTION_SEPARATOR);
            StringBuilder answers = new StringBuilder();
            String questionId = answerElements.get(0).attr("data-question-name");
            for (int i = 0; i < results.length; i++) {
                answers.append(answerElements.get(results[i].charAt(0) - 'A')
                        .attr("data-question-value")
                );
                if (i != results.length -1) {
                    answers.append("|");
                }
            }
            if (apiMapper.getQuestionCount(questionId, examId) <= 0) {
                apiMapper.addQuestion(new Question(
                        questionId,
                        examId,
                        answers.toString()
                ));
            }
        }
    }

    @Override
    public void close() throws IOException {
        MyBatisHelper.getInstance().close();
    }
}
