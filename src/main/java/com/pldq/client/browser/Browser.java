package com.pldq.client.browser;

import com.pldq.client.browser.handlers.CefContextMenuHandler;
import com.pldq.client.browser.handlers.MessageRouterHandler;
import org.cef.CefApp;
import org.cef.CefClient;
import org.cef.CefSettings;
import org.cef.OS;
import org.cef.browser.CefBrowser;
import org.cef.browser.CefMessageRouter;
import org.cef.handler.CefAppHandlerAdapter;

import java.awt.*;

/**
 * @author pldq
 * @date 2018-11-26
 */
public class Browser {

    private final CefApp cefApp;
    private final CefClient cefClient;
    private final CefBrowser cefBrowser;

    public Browser(String address) {
        CefApp.addAppHandler(new CefAppHandlerAdapter(null) {
            @Override
            public void stateHasChanged(CefApp.CefAppState cefAppState) {
                if (cefAppState == CefApp.CefAppState.TERMINATED) {
                    System.exit(0);
                }
            }
        });
        CefSettings cefSettings = new CefSettings();
        cefSettings.windowless_rendering_enabled = OS.isLinux();
        cefApp = CefApp.getInstance(cefSettings);
        cefClient = cefApp.createClient();
        CefMessageRouter cefMessageRouter = CefMessageRouter.create();
        cefMessageRouter.addHandler(new MessageRouterHandler(), true);
        cefClient.addMessageRouter(cefMessageRouter);
        cefClient.addContextMenuHandler(new CefContextMenuHandler());
        cefBrowser = cefClient.createBrowser(
                address,
                OS.isLinux(),
                false
        );
    }

    public Component getBrowserComponent() {
        return cefBrowser.getUIComponent();
    }

    public void dispose() {
        cefApp.dispose();
    }

    public void loadURL(String address) {
        cefBrowser.loadURL(address);
    }

    public CefBrowser getCefBrowser() {
        return cefBrowser;
    }
}
