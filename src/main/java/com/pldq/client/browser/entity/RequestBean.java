package com.pldq.client.browser.entity;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Package: com.pldq.client.browser.entity
 *
 * @author pldq
 * Created on 2018-12-17
 */
@Setter
@Getter
@ToString
public class RequestBean {

    private String command;
    private String body;

}
