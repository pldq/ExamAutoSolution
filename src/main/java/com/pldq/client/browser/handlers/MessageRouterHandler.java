package com.pldq.client.browser.handlers;

import com.google.gson.Gson;
import com.pldq.client.bootstrap.BootstrapLoader;
import com.pldq.client.browser.entity.RequestBean;
import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.callback.CefQueryCallback;
import org.cef.handler.CefMessageRouterHandlerAdapter;

import java.util.HashMap;

/**
 * Package: com.pldq.client.browser.handlers
 *
 * @author pldq
 * Created on 2018-12-16
 */
public class MessageRouterHandler extends CefMessageRouterHandlerAdapter {

    private Gson gson;

    public MessageRouterHandler() {
        gson = new Gson();
    }

    @Override
    public boolean onQuery(CefBrowser browser, CefFrame frame, long queryId, String request, boolean persistent, CefQueryCallback callback) {
        RequestBean requestBean = gson.fromJson(request, RequestBean.class);
        System.out.println("queryId = " + queryId + ", command = " + requestBean.getCommand() + ", body = " + requestBean.getBody());
        return BootstrapLoader.getBootstrapLoader()
                .getHandlerMapping()
                .handle(requestBean, callback);
    }

    @Override
    public void onQueryCanceled(CefBrowser browser, CefFrame frame, long queryId) {
    }
}
