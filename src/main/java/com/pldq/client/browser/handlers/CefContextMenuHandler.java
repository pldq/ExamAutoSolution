package com.pldq.client.browser.handlers;

import org.cef.browser.CefBrowser;
import org.cef.browser.CefFrame;
import org.cef.callback.CefContextMenuParams;
import org.cef.callback.CefMenuModel;
import org.cef.handler.CefContextMenuHandlerAdapter;

/**
 * @author pldq
 * @date 2018-11-26
 */
public class CefContextMenuHandler extends CefContextMenuHandlerAdapter {
    @Override
    public void onBeforeContextMenu(CefBrowser cefBrowser, CefFrame cefFrame, CefContextMenuParams cefContextMenuParams, CefMenuModel cefMenuModel) {
        super.onBeforeContextMenu(cefBrowser, cefFrame, cefContextMenuParams, cefMenuModel);
        cefMenuModel.clear();
    }
}
