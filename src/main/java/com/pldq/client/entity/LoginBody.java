package com.pldq.client.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * Package: com.pldq.client.entity
 *
 * @author pldq
 * Created on 2018-12-18
 */
@Getter
@Setter
public class LoginBody {

    private String username;
    private String password;

}
