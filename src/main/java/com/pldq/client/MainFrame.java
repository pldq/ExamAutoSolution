package com.pldq.client;

import com.google.gson.Gson;
import com.pldq.client.bootstrap.BootstrapLoader;
import com.pldq.client.browser.Browser;
import com.pldq.client.browser.entity.RequestBean;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Objects;

/**
 * @author pldq
 * @date 2018-11-26
 */
public class MainFrame extends JFrame {

    private final Browser browser;

    private MainFrame() throws HeadlessException {
        URL url = getClass().getClassLoader().getResource("static/login.html");
        Objects.requireNonNull(url);
        browser = new Browser(url.getProtocol() + "://" + url.getFile());
        setBounds(0, 0, 800, 600);
        setLocationRelativeTo(null);
        setUndecorated(false);
        setResizable(false);
        setVisible(true);
        add(browser.getBrowserComponent());
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                browser.dispose();
                dispose();
            }
        });
    }

    public static void main(String[] args) {
        BootstrapLoader.run(MainFrame.class);
        EventQueue.invokeLater(MainFrame::new);
    }

}
