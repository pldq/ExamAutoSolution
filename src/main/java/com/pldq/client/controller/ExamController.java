package com.pldq.client.controller;

import com.google.gson.Gson;
import com.pldq.client.bootstrap.annotation.Controller;
import com.pldq.client.bootstrap.annotation.RequestMapping;
import com.pldq.client.entity.LoginBody;
import com.pldq.core.ExamSolution;
import com.pldq.core.entity.Group;
import org.cef.callback.CefQueryCallback;

import java.io.IOException;
import java.util.List;

/**
 * Package: com.pldq.client.controller
 *
 * @author pldq
 * Created on 2018-12-18
 */
@Controller
public class ExamController {

    private ExamSolution examSolution;

    private Gson gson = new Gson();

    public ExamController() {
    }

    @RequestMapping(command = "login")
    public void login(LoginBody loginBody, CefQueryCallback callback) throws IOException {
        examSolution = new ExamSolution(loginBody.getUsername(), loginBody.getPassword());
        if (examSolution.login()) {
            callback.success("");
        } else {
            callback.failure(-1, "");
        }
    }

    @RequestMapping(command = "groups")
    public void listGroups(CefQueryCallback callback) throws IOException {
        List<Group> groupList = examSolution.listGroup();
        callback.success(gson.toJson(groupList));
    }

    @RequestMapping(command = "workForGroup")
    public void workForGroup(String groupId, CefQueryCallback callback) {
        try {
            examSolution.work(groupId);
            callback.success("");
        } catch (Exception e) {
            callback.failure(-1, e.getLocalizedMessage());
        }
    }
}
