package com.pldq.client.bootstrap;

import com.pldq.client.bootstrap.mapping.DefaultHandlerMapping;
import com.pldq.client.bootstrap.mapping.HandlerMapping;

/**
 * Package: com.pldq.client
 *
 * @author pldq
 * Created on 2018-12-18
 */
public class BootstrapLoader {

    private static BootstrapLoader bootstrapLoader;

    private HandlerMapping handlerMapping;

    private BootstrapLoader() {
    }

    public static void run(Class<?> runnerClass) {
        bootstrapLoader = new BootstrapLoader();
        bootstrapLoader.setHandlerMapping(new DefaultHandlerMapping(runnerClass.getPackage().getName()));
    }

    public static BootstrapLoader getBootstrapLoader() {
        return bootstrapLoader;
    }

    public static void setBootstrapLoader(BootstrapLoader bootstrapLoader) {
        BootstrapLoader.bootstrapLoader = bootstrapLoader;
    }

    public HandlerMapping getHandlerMapping() {
        return handlerMapping;
    }

    private void setHandlerMapping(HandlerMapping handlerMapping) {
        this.handlerMapping = handlerMapping;
    }

}
