package com.pldq.client.bootstrap.utils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

/**
 * Package: com.pldq.client.bootstrap.utils
 *
 * @author pldq
 * Created on 2018-12-17
 */
public class ClassUtil {

    public static List<Class<?>> getClassFromPackage(String packageName) {
        LinkedList<Class<?>> classes = new LinkedList<>();
        try {
            Enumeration<URL> dirs = Thread.currentThread()
                    .getContextClassLoader()
                    .getResources(packageName.replace(".", "/"));
            while (dirs.hasMoreElements()) {
                URL url = dirs.nextElement();
                String protocol = url.getProtocol();
                if ("file".equals(protocol)) {
                    String path = URLDecoder.decode(url.getFile(), "UTF-8");
                    findAllClassByFile(packageName, path, classes);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return classes;
    }

    private static void findAllClassByFile(String packageName, String path, List<Class<?>> classes) {
        File directory = new File(path);
        if (!directory.exists() || !directory.isDirectory()) {
            return;
        }
        File[] files = directory.listFiles(pathname ->
                pathname.isDirectory() || pathname.getName().endsWith(".class")
        );
        if (files == null) {
            return;
        }
        for (File file : files) {
            if (file.isDirectory()) {
                findAllClassByFile(packageName + "." + file.getName(), file.getAbsolutePath(), classes);
            } else {
                String className = packageName + "." + file.getName().substring(0, file.getName().length() - 6);
                try {
                    classes.add(Class.forName(className));
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
