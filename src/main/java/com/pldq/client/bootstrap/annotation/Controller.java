package com.pldq.client.bootstrap.annotation;

import java.lang.annotation.*;

/**
 * Package: com.pldq.client.bootstrap.annotation
 *
 * @author pldq
 * Created on 2018-12-18
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Controller {
}
