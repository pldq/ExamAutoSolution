package com.pldq.client.bootstrap.mapping;

import com.pldq.client.browser.entity.RequestBean;
import org.cef.callback.CefQueryCallback;

/**
 * Package: com.pldq.client.bootstrap.mapping
 *
 * @author pldq
 * Created on 2018-12-18
 */
public interface HandlerMapping {

    boolean handle(RequestBean requestBean, CefQueryCallback callback);

}
