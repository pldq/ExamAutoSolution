package com.pldq.client.bootstrap.mapping;

import com.google.gson.Gson;
import com.pldq.client.bootstrap.annotation.Controller;
import com.pldq.client.bootstrap.annotation.RequestMapping;
import com.pldq.client.bootstrap.utils.ClassUtil;
import com.pldq.client.browser.entity.RequestBean;
import org.cef.callback.CefQueryCallback;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * Package: com.pldq.client.bootstrap.mapping
 *
 * @author pldq
 * Created on 2018-12-18
 */
public class DefaultHandlerMapping implements HandlerMapping {

    private HashMap<Class<?>, Object> controllers;
    private Gson gson;

    public DefaultHandlerMapping(String packageName) {
        controllers = new HashMap<>();
        gson = new Gson();
        for (Class<?> item : ClassUtil.getClassFromPackage(packageName)) {
            if (item.getAnnotation(Controller.class) == null) {
                continue;
            }
            try {
                Object object = item.newInstance();
                controllers.put(item, object);
                System.out.println("controller = " + item.getSimpleName());
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean handle(RequestBean requestBean, CefQueryCallback callback) {
        for (Class<?> controller : controllers.keySet()) {
            for (Method declaredMethod : controller.getDeclaredMethods()) {
                RequestMapping requestMapping = declaredMethod.getAnnotation(RequestMapping.class);
                if (requestMapping == null || !requestMapping.command().equals(requestBean.getCommand())) {
                    continue;
                }
                Object[] parameters = new Object[declaredMethod.getParameterCount()];
                for (int i = 0; i < declaredMethod.getParameterCount(); i++) {
                    Class<?> parameterClass = declaredMethod.getParameterTypes()[i];
                    if (CefQueryCallback.class.equals(parameterClass)) {
                        parameters[i] = callback;
                    } else {
                        parameters[i] = gson.fromJson(requestBean.getBody(), parameterClass);
                    }
                }
                try {
                    declaredMethod.invoke(controllers.get(controller), parameters);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
                return true;
            }
        }
        return false;
    }
}
